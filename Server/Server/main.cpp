#include <iostream>
#include <WS2tcpip.h>
#include <string>
#include <sstream>

#pragma comment (lib, "ws2_32.lib")

using namespace std;

int main()
{
	// Initialze winsock
	WSADATA wsData;
	WORD ver = MAKEWORD(2, 2);

	int wsOk = WSAStartup(ver, &wsData);
	if (wsOk != 0)
	{
		cerr << "Can't Initialize winsock! Quitting" << endl;
		return 99;
	}

	// Create a socket
	SOCKET listening = socket(AF_INET, SOCK_STREAM, 0);
	if (listening == INVALID_SOCKET)
	{
		cerr << "Can't create a socket! Quitting" << endl;
		return 99;
	}

	// Bind the ip address and port to a socket
	sockaddr_in hint;
	hint.sin_family = AF_INET;
	hint.sin_port = htons(54000);
	hint.sin_addr.S_un.S_addr = INADDR_ANY;

	bind(listening, (sockaddr*)&hint, sizeof(hint));

	// Tell Winsock the socket is for listening 
	listen(listening, SOMAXCONN);

	// Create the master file descriptor set and zero it
	fd_set master;
	FD_ZERO(&master);

	// Add our first socket that we're interested in interacting with; the listening socket!
	FD_SET(listening, &master);

	bool running = true;

	while (running)
	{
		// Make a copy of the master file descriptor set.
		// When passing 'copy' into select(), only the sockets that are interacting with the server are returned. 
		fd_set copy = master;

		// See who's talking to us
		int socketCount = select(0, &copy, nullptr, nullptr, nullptr);

		// Loop through all the current connections connect
		for (int i = 0; i < socketCount; i++)
		{
			SOCKET sock = copy.fd_array[i];

			if (sock == listening)
			{
				// Accept a new connection
				SOCKET client = accept(listening, nullptr, nullptr);

				// Add the new connection to the list of connected clients
				FD_SET(client, &master);

				// Send a message to all connected clients
				string welcomeMsg = "SERVER:Welcome " + to_string(client) + "  to the Awesome Chat Server!";
				send(client, welcomeMsg.c_str(), welcomeMsg.size() + 1, 0);
				for (u_int i = 0; i < master.fd_count; i++)
				{
					SOCKET outSock = master.fd_array[i];
					if (outSock == listening)
					{
						continue;
					}

					ostringstream ss;
					ss << "SOCKET #" << client << " connected succesfully! " << "\r\n";

					string strOut = ss.str();
					send(outSock, strOut.c_str(), strOut.size() + 1, 0);
				}
			}
			else 
			{
				char buf[4096];
				ZeroMemory(buf, 4096);

				// Receive message
				int bytesIn = recv(sock, buf, 4096, 0);
				if (bytesIn <= 0)
				{
					// Drop the client
					closesocket(sock);
					FD_CLR(sock, &master);
				}
				else
				{

					if (buf[0] == 'p') {
						//sending private message
						char socketID[3];
						socketID[0] = buf[2];
						socketID[1] = buf[3];
						socketID[2] = buf[4];
						int privateMsgSocket = atoi(socketID);
						for (u_int i = 0; i < master.fd_count; i++)
						{
							if (master.fd_array[i] == privateMsgSocket) {
								string message;
								for (int a = 6; buf[a] != '\n'; ++a) {
									message += buf[a];
								}
								ostringstream ss;
								ss << "PRIVATE MSG FROM:SOCKET #" << sock << ":" << message << "\r\n";
								string strOut = ss.str();
								send(master.fd_array[i], strOut.c_str(), strOut.size() + 1, 0);
								break;
							}
						}
					}
					else  {

						// Send message to other clients

						for (u_int i = 0; i < master.fd_count; i++)
						{
							SOCKET outSock = master.fd_array[i];
							if (outSock == listening)
							{
								continue;
							}

							ostringstream ss;

							string message;
							for (int a = 6; buf[a] != '\n'; ++a) {
								message += buf[a];
							}

							if (outSock != sock)
							{
								ss << "SOCKET #" << sock << ": " << message << "\r\n";
							}
							else
							{
								ss << "ME: " << message << "\r\n";
							}

							string strOut = ss.str();
							send(outSock, strOut.c_str(), strOut.size() + 1, 0);
						}
					}
				}
			}
		}
	}

	// Remove the listening socket from the master file descriptor set and close it
	// to prevent anyone else trying to connect.
	FD_CLR(listening, &master);
	closesocket(listening);

	// Message to let users know what's happening.
	string msg = "SERVER:Server is shutting down. Goodbye\r\n";

	while (master.fd_count > 0)
	{
		// Get the socket number
		SOCKET sock = master.fd_array[0];

		// Send the goodbye message
		send(sock, msg.c_str(), msg.size() + 1, 0);

		// Remove it from the master file list and close the socket
		FD_CLR(sock, &master);
		closesocket(sock);
	}

	// Cleanup winsock
	WSACleanup();
	return 0;
}